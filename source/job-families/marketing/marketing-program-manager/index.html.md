---
layout: job_family_page
title: "Marketing Program Manager"
---
## Intro

You might describe yourself as analytical, creative, organized, and curious. You have experience in planning engaging marketing programs and using modern marketing technology to deliver those programs. You enjoy setting up elegant workflow in marketing automation platforms, conducting quality assurance, measuring results and making informed decisions based on what you learn.

## Responsibilities

* The marketing program manager will report to the Manager of Digital Marketing Programs.
* Manage business-to-business marketing programs in Marketo for email campaigns, event promotion, event follow up, drip email nurture series, webinars, and content.
    * Work closely with content and design teams to create landing pages for webinar registration and gated content.
    * Maintain marketing database segmentation to ensure any communications delivered are relevant to the audience receiving them.
    * Work closely with product marketing on mapping marketing database segmentation to personas.
    * Ensure all Marketo programs are linked to salesforce.com campaigns to ensure accurate campaign ROI tracking.
    * Analyze email, webinar, and campaign performance with an eye towards continuous improvement, and sharing insights across the team.
    * Facilitate a marketing program retrospective following completion of each marketing program.
* Create email and landing page split tests to improve the team's understanding of what subject lines, headers, images, copy, and calls-to-action are most effective.
* Work with our marketing and sales teams to become an expert on the company's lead funnel - from web visitor to customer and everything in-between.
* Participate in marketing technology evaluation.
* Document all processes in the handbook and update as needed.

### Senior Responsibilities

* Create marketing automation enabled marketing campaigns. 
* Perform marketing data analysis and market segmentation. Perform customer data analysis, ideal customer profile definition, and lead scoring model creation.  
* Build out marketing database segmentation to ensure any communications delivered are relevant to the audience receiving them and work closely with product marketing on mapping marketing database segmentation to personas.  

## Requirements

* Excellent spoken and written English.
* Deep empathy for our audience of developers and IT leaders. Our community is changing the world, and you want to help accelerate their success through relevant, useful communications.
* You are obsessed with making customers happy. You know that spam, unclear directions, and unnecessary clicks/forms/inputs can be incredibly annoying.
* Power user of marketing automation software (Marketo preferred).
* Power user of CRM software (Salesforce preferred).
* Experience working in a B2B software marketing team.
* Experience with Open Source software, and/or the developer tools space is preferred.
* Proficiency in MS Excel or Google Sheets.
* Be familiar with or ready to learn how to use GitLab and Git.
* You share our [values](/handbook/values), and work in accordance with those values.

### Additional Senior Role Requirements:

* Bachelor’s degree or foreign equivalent in Business Administration, Marketing, Finance, or related field, and 72 months of relevant experience.


## Manager, Marketing Programs
The Manager of Marketing Programs for GitLab should have a background and hands-on experience in all areas of marketing programs: campaign organization and planning, email marketing, marketing operations, ABM, and integrated campaigns. They will have experience managing marketing teams and working with all teams within the larger marketing department. They should also be able to maintain a budget, use analytics tools, CRM, MAT, and other marketing tools, and have a data-driven approach to marketing. Experience in marketing operations and marketing project management are also helpful in this role.

### Responsibilities
* Develop and execute high-impact, integrated B2B demand generation programs to achieve revenue and pipeline goals.
* Strategize campaign calendar and delegation of campaign planning within the marketing programs team.
* Work cross-functionally across the marketing organization as well as sales and other teams to design and orchestrate impactful, targeted marketing programs.
* Manage, build, and lead a strong team by coaching and developing existing members and closing talent gaps where needed through acquisition of new team members.
* Build and implement marketing programs strategy—understand key partnerships within organization to drive full funnel of customer acquisition, retention, and upsell.
* Lead effort to create standardized marketing reporting and dashboards, presenting performance to the marketing department as well as stakeholders throughout the company.
* Oversee our campaign calendar and communicate to marketing team and larger organization, maximizing for return on marketing spend.
* [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#management-group)
### Requirements
* 5-7 years of enterprise software marketing experience.
* 2+ years of experience hiring, onboarding, and managing marketing teams.
* Driven self-starter with proven track record of project managing and executing integrated marketing campaigns.
* Background in driving targeted acquisition, retention, upsell, and cross-sell marketing campaigns.
* Strong skills in demand generation, marketing programs, marketing operations, and reporting.
* Experience working cross-functionally with sales development to drive cohesive engagement with marketing-driven leads.
* Ability to construct lifecycle reports and dashboards in Salesforce and other reporting tools.
* Excellent organizational skills to juggle many tasks without losing sight of the highest priority items.
* Power user of Marketo and Salesforce.
* Strong communication and presentation skills, including to executive leadership
* Strong interpersonal skills and ability to influence and motivate people at all levels across a broad variety of job functions.



## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team/).

- Qualified candidates receive a short questionnaire from our Global Recruiters.
  1. What would differentiate you and make you a great marketing program manager for GitLab?
  1. What is your knowledge of the space that GitLab is in? (e.g. industry trends).
  1. Generally, how would you describe the communication preferences of developers and technical IT management?
- Selected candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with one of our Global Recruiters.
- Next, candidates will be invited to schedule a series of 45 minute interviews with team members Digital Marketing, Marketing Programs, Marketing Ops, and Revenue Marketing.
- Candidates will then be invited to schedule 45 minute interviews with our Senior Director of Reevnue Marketing and possibly our CMO.
- Finally, our CEO may choose to conduct a final interview.
- Successful candidates will subsequently be made an offer via email.

Additional details about our process can be found on our [hiring page](/handbook/hiring).
