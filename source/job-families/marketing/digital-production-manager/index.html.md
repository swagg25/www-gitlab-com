---
layout: job_family_page
title: "Digital Production"
---

## Digital Media Associate Producer

### Responsibilities

- Liaise with internal teams to assist with pre-production for GitLab branded audio and video projects and events, including but not limited to Contribute, Connect events, Commit user conference, Technical Evangelism, Community events, Employment branding, sales enablement, social and livestreams.
- Write and/or edit treatments and scripts for multimedia packages.
- Pitch ideas for customer and user stories as well as promotional videos.
- Assist with the management of GitLab YouTube channels.
- Travel required.
- Basic video editing skills a plus.

## Digital Production Manager 

### Responsibilities

- Develop multimedia (audio/video) content for brand and marketing initiatives.
- Plan and manage production for projects; write treatments, determine scope, scout and select locations and talent, coordinate shoots.
- Manage creative projects, including scripting, storyboarding, and graphic elements, to ensure brand consistency.
- Direct video shoots and manage production crew.
- Collaborate closely with internal clients and stakeholders to assess and execute production needs.
- Execute video & audio production for GitLab corporate events. 
- Manage video library.

## Requirements
- 5+ years of experience in video and audio production
- Excellent communication, storytelling, and presentation of skills
- Strong organizational skills, ability to handle projects end-to-end
- Proficiency with video and audio software and equipment
- You share our values, and work in accordance with those values.
- BONUS: A passion and strong understanding of the industry and our mission.


## Manager, Digital Production

## Responsibilities 

- Manage digital production team. 
- Develop multimedia (audio/video) strategy for brand and marketing initiatives.
- Set and manage the quartlerly and bi-annual production deliverables and schedule. 
- Manage digital production budget, vendors, schedules, and equipment. 
- Plan and manage production; write treatments, determine scope, scout and select locations and talent, coordinate shoots. 
- Manage creative, including scripting, storyboarding, and graphic elements, to ensure brand consistency. 
- Direct video shoots and manage production crew. 
- Collaborate closely with internal clients and stakeholders to assess and prioritize production needs.
- Manage audio and video production for GitLab corporate events.
- Report on production metrics.

## Requirements 

- 5+ years of experience in video and audio production 
- Previous management experience 
- Proven ability to manage production budgets and timelines
- Excellent communication, storytelling, and presentation of skills
- Strong organizational skills, ability to handle projects end-to-end
- Proficiency with video and audio software and equipment 
- You share our [values](/handbook/values), and work in accordance with those values.
- BONUS: A passion and strong understanding of the industry and our mission. 
